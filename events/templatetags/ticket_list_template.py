from django import template
from ..forms import EventForm, TicketForm

register = template.Library()

@register.inclusion_tag('events/ticket.html')
def ticket_list_template(ticketId, ticketTitle, ticketPrice, ticketStartDate, ticketEndDate):
	return {
		'ticket_id':ticketId,
		'ticket_price':ticketPrice,
		'ticket_title':ticketTitle,
		'ticket_start_date':ticketStartDate,
		'ticket_end_date':ticketEndDate,
	}