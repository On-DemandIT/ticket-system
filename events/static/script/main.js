var randomTicketId = 1;
var eventDate = null;
var initializeEventDatePicker = function(eventStartDate, eventEndDate){
	$("#event-start-date-picker").datetimepicker({
		format: "MM-DD-YYYY hh:mm a",
		minDate: (eventStartDate && moment() < eventStartDate) ? moment() : eventStartDate,
		sideBySide: true
	});
    $("#event-end-date-picker").datetimepicker({
    	format: "MM-DD-YYYY hh:mm a",
        useCurrent: false,
        sideBySide: true
    });
   
    $("#event-start-date-picker").on("dp.change", function (e) {
    	$("#event-end-date-picker").data("DateTimePicker").minDate(e.date);
    });

    $("#event-end-date-picker").on("dp.change", function (e) {
        $("#event-start-date-picker").data("DateTimePicker").maxDate(e.date);
    });
}

var initializeTicketDatePicker = function(currentCounter, eventStartDate, eventEndDate){
	
	$("#ticket-start-date-" + currentCounter).datetimepicker({
		format: "MM-DD-YYYY hh:mm a",
		minDate: (moment() < eventStartDate) ? moment() : eventStartDate,
		sideBySide: true               
	});

    $("#ticket-end-date-" + currentCounter).datetimepicker({
    	format: "MM-DD-YYYY hh:mm a",
    	useCurrent: true,
    	sideBySide: true
    });

    if(eventEndDate) {
    	$("#ticket-end-date-" + currentCounter).data("DateTimePicker").minDate(moment());
    	
		$("#ticket-start-date-" + currentCounter).data("DateTimePicker").maxDate(eventEndDate);
		$("#ticket-end-date-" + currentCounter).data("DateTimePicker").maxDate(eventEndDate);
	}

    $("#ticket-start-date-" + currentCounter).on("dp.change", function (e) {
    	$("#ticket-end-date-" + currentCounter).data("DateTimePicker").minDate(e.date);
    });

    $("#ticket-end-date-" + currentCounter).on("dp.show", function (e) {
    	$("#id-ticket-end-date-" + currentCounter).val(moment($("#id-ticket-end-date-" + currentCounter).val() , "MM-DD-YYYY hh:mm a").format("MM-DD-YYYY") + " 00:00 AM");
    });

	randomTicketId++;
}

function deleteTicket(self, currentSection) {
	bootbox.confirm({
			title: "<h3>Confirmation</h3>",
			message : "<h3>Do you want to delete this ticket?</h3>", 
			buttons: {
			        cancel: {
				            label: '<i class="fa fa-times"></i> Cancel'
			        },
			        confirm: {
			            label: '<i class="fa fa-check"></i> Confirm'
			        }
	    	},
    		callback: function (result) {
    			if(result){
					$("#ticket-section-" + currentSection).remove();

					if(!$("#ticket-section").html().trim()){
						$("#ticket-section").html("<div align='center' class='initial-message'><b>Ticket Goes Here</b></div>");
					}
				}
			}
		});
}	

function addTicket(){
	var eventStartDate = moment($("#id_event_start_date").val(), "MM-DD-YYYY hh:mm A");
	var eventEndDate = moment($("#id_event_end_date").val(), "MM-DD-YYYY hh:mm A");
	
	if($("#ticket-section").find('.initial-message').length) {
		$("#ticket-section").html("");
	}

	if(!$("#id_event_start_date").val().trim() || !$("#id_event_end_date").val().trim()) {
		bootbox.alert({
			title: "<h3>Invalid Request</h3>",
			message : "<h3>Please add event before any ticket!</h3>", 
			
		})
		return;
	}

	$.get('/events/ticket/'+ randomTicketId + '/', function(response) {
		$("#ticket-section").append(response).promise().done(initializeTicketDatePicker(randomTicketId, eventStartDate, eventEndDate));
	})
}

function deleteEvent(eventId){
	bootbox.confirm({
			title: "<h3>Confirmation</h3>",
			message : "<h3>Do you want to delete this event?</h3>", 
			buttons: {
			        cancel: {
				            label: '<i class="fa fa-times"></i> Cancel'
			        },
			        confirm: {
			            label: '<i class="fa fa-check"></i> Confirm'
			        }
	    	},
    		callback: function (result) {
    			if(result){
    				window.location = "/events/delete/" + eventId;
    			}
			}
		});
}


function addConfirmation(){
	if(isValidForm()) {
		bootbox.confirm({
				title: "<h3>Add event</h3>",
				message : "<div align='center'><h3>This will add and publish event. Do you want to continue?</h3></div>", 
				buttons: {
				        cancel: {
					            label: '<i class="fa fa-times"></i> Cancel'
				        },
				        confirm: {
				            label: '<i class="fa fa-check"></i> Publish'
				        }
		    	},
	    		callback: function (result) {
	    			if(result){
	    				parseDate(true);
	    				$("#event-form").submit();
	    			}
				}
			});
	} else {
		bootbox.alert({
			title: "<h3>Invalid Request</h3>",
			message : "Please add event before submittion", 
			size: 'small'
		})
	}
}

function editConfirmation(){
	if(isValidForm()) {
		bootbox.confirm({
				title: "<h3>Add event</h3>",
				message : "<div align='center'><h3>This will update current event. Do you want to continue?</h3></div>", 
				buttons: {
				        cancel: {
					            label: '<i class="fa fa-times"></i> Cancel'
				        },
				        confirm: {
				            label: '<i class="fa fa-check"></i> Update'
				        }
		    	},
	    		callback: function (result) {
	    			if(result){
	    				parseDate(true);
	    				$("#event-form").submit();
	    			}
				}
			});
	} else {
		bootbox.alert({
			title: "<h3>Invalid Request</h3>",
			message : "Please enter valid event with tickets", 
			size: 'small'
		})
	}
}


function isValidForm(){
	var validForm = true;
	$(".form-control").each(function(){
		if( !$(this).val().trim() ){
			validForm = false;
			$(this).addClass('error-field');
		} else {
			$(this).removeClass('error-field');
		}
	})

	if(validForm) {
		$(".picker-input").each(function(){
			if (!Date.parse($(this).val().trim())) {
				validForm = false;
				$(this).addClass('error-field');
			} else {
				$(this).removeClass('error-field');
			}	
		})
	}

	if($(".price").length){
		if($(".price").val().trim() && isNaN($(".price").val().trim())) {
			validForm = false;
			$(".price").addClass('error-field');
		} 
	}

	return validForm;
}

function parseDate(validForm) {
	if(validForm) {
		$(".picker-input").each(function(){
			var newDate = (moment($(this).val(), "MM-DD-YYYY hh:mm A").format("MM-DD-YYYY HH:mm"));
			$(this).val(newDate);
		})
	}
}


function checkValidDate(){
	console.log("changed");
}
