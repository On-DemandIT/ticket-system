from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.utils.dateparse import parse_date
from django.shortcuts import get_object_or_404, render
from .models import Event, Ticket
from .forms import EventForm, TicketForm
from datetime import datetime
import json
import sys



def _getEventData(eventDetail):
	eventData = {}
	for event in eventDetail:
		eventData = {
			'event_title':event.event_title,
			'event_start_date':event.event_start_date,
			'event_end_date':event.event_end_date
		}
	return eventData

def _getTicketData(ticketDetail):
	ticketData = []
	for ticket in ticketDetail:
			ticketData.append({
				'ticket_id':(ticket.id),
				'ticket_price':ticket.ticket_price, 
				'ticket_title':(ticket.ticket_title),
				'ticket_start_date':ticket.sale_start_date,
				'ticket_end_date':ticket.sale_end_date
			})
	return ticketData	

def index(request):
    return HttpResponseRedirect('/events/list/')

def add(request):
	template = loader.get_template('events/add.html')
	ticketList = []
	
	if request.method == 'POST':
		eventForm = EventForm(request.POST)
		event_start_date = request.POST['event_start_date']
		event_start_date = datetime.strptime(event_start_date, "%m-%d-%Y %H:%M")
		event_end_date = request.POST['event_end_date']
		event_end_date = datetime.strptime(event_end_date, "%m-%d-%Y %H:%M")
		
		context = {'message' : '', 'ticket_list': [], 'eventForm': eventForm}
				
		if eventForm.is_valid():
			ticket_title_list = request.POST.getlist('ticket[][ticket_title]')
			ticket_price_list = request.POST.getlist('ticket[][ticket_price]')
			ticket_start_date_list = request.POST.getlist('ticket[][ticket_start_date]')
			ticket_end_date_list = request.POST.getlist('ticket[][ticket_end_date]')
			
			for index, ticket in enumerate(ticket_title_list):
				ticketList.append({
					'ticket_id': index + 1,
					'ticket_title':ticket,
					'ticket_price':ticket_price_list[index],
					'ticket_start_date':datetime.strptime(ticket_start_date_list[index], '%m-%d-%Y %H:%M'),
					'ticket_end_date':datetime.strptime(ticket_end_date_list[index], '%m-%d-%Y %H:%M')
					})
				
			previousEvent = Event.objects.filter(event_title=request.POST['event_title']).count()
			
			if(previousEvent):
				context = {'message' : 'Event with name - "' + request.POST['event_title'] + '" allready exist', 'ticket_list': ticketList, 'event_form': eventForm}
			else:
				event = eventForm.save_event()
				
				for index, ticket in enumerate(ticket_title_list):
					ticketModel = Ticket(ticket_title=ticket,
						ticket_price=ticket_price_list[index],
						sale_start_date=datetime.strptime(ticket_start_date_list[index], "%m-%d-%Y %H:%M"),
						sale_end_date=datetime.strptime(ticket_end_date_list[index], "%m-%d-%Y %H:%M"),
						event=event)
					ticketModel.save()
			
				return HttpResponseRedirect('/events/thanks/1/')
	else:
		eventForm = EventForm()
		context = {'message' : '', 'ticket_list': [], 'event_form': eventForm}

	return HttpResponse(template.render(context, request))

def edit(request, event_id):
	eventDetail = Event.objects.filter(id=event_id)
	ticketDetail = Ticket.objects.filter(event_id=event_id)
	ticketList = []

	if (Event.objects.filter(id=event_id).count()) != 1:
		return render(request, 'events/page-not-found.html', {})

	if request.method == 'POST':
			ticket_title_list = request.POST.getlist('ticket[][ticket_title]')
			ticket_price_list = request.POST.getlist('ticket[][ticket_price]')
			ticket_start_date_list = request.POST.getlist('ticket[][ticket_start_date]')
			ticket_end_date_list = request.POST.getlist('ticket[][ticket_end_date]')

			for index, ticket in enumerate(ticket_title_list):
				ticketList.append({
					'ticket_title':ticket,
					'ticket_price':ticket_price_list[index],
					'sale_start_date':datetime.strptime(ticket_start_date_list[index], '%m-%d-%Y %H:%M'),
					'sale_end_date':datetime.strptime(ticket_end_date_list[index], '%m-%d-%Y %H:%M')
				})

			event_title = request.POST.get('event_title',"")
			event_start_date =  request.POST.get('event_start_date', "")
			event_start_date = datetime.strptime(event_start_date, '%m-%d-%Y %H:%M')
			event_end_date =  request.POST.get('event_end_date', "")
			event_end_date = datetime.strptime(event_end_date, '%m-%d-%Y %H:%M')

			eventRow = Event.objects.get(id=event_id)

			if eventRow.event_title != event_title:
				previousEvent = Event.objects.filter(event_title=request.POST.get('event_title',"")).exclude(id=event_id).count()
				if(previousEvent):
					eventData = _getEventData(eventDetail)
					ticketList = _getTicketData(ticketDetail)
					eventForm = EventForm(initial=eventData)
					ticketForm = TicketForm()
					context = {'message' : 'Event with name -"' + request.POST['event_title'] + '" allready exist', 'event_id': event_id, 'ticket_list': ticketList, 'event_form': eventForm,  'event_data': eventData}
					return render(request, 'events/edit.html', context)	
					
			eventRow.event_title = event_title
			eventRow.event_start_date  = event_start_date
			eventRow.event_end_date  = event_end_date
			eventRow.save()
			
			# Clear previous tickets
			if Event.objects.filter(id=event_id).count():
				ticketDetail.delete()
			
			if ticket_title_list:
				for index, ticket in enumerate(ticket_title_list):
						ticketModel = Ticket(ticket_title=ticket,
							ticket_price=ticket_price_list[index],
							sale_start_date=datetime.strptime(ticket_start_date_list[index], "%m-%d-%Y %H:%M"),
							sale_end_date=datetime.strptime(ticket_end_date_list[index], "%m-%d-%Y %H:%M"),
							event=eventRow)
						ticketModel.save()
			
			return HttpResponseRedirect('/events/thanks/2/')
	else:		
		eventData = _getEventData(eventDetail)
		ticketList = _getTicketData(ticketDetail)
		
		eventForm = EventForm(initial=eventData)
		ticketForm = TicketForm()
		context = {'message' : '', 'event_id': event_id, 'ticket_list': ticketList, 'event_data': eventData, 'event_form': eventForm, 'ticket_form':ticketForm}
		return render(request, 'events/edit.html', context)	


def list(request):
	context = {'event_list' : Event.objects.all()}
	return render(request, 'events/list.html', context)

def detail(request, event_id):
	context = {'event_detail' : Event.objects.filter(id=event_id), 'ticket_detail': Ticket.objects.filter(event_id=event_id)}
	return render(request, 'events/detail.html', context)

def delete(request, event_id):
	event = Event.objects.filter(id=event_id)
	event.delete()
	return HttpResponseRedirect('/events/list/')


def thanks(request, request_type):
	if int(request_type) == 1 :
		context = {'message' : 'Event has been created'}
	else:
		context = {'message' : 'Event has been updated'}
	return render(request, 'events/thanks.html', context)

def ticket(request, ticket_id):
	context = {'ticket_id' : ticket_id}
	return render(request, 'events/ticket.html', context)		

def bad_request(request):
	return render(request, 'events/bad-request.html', {})

def permission_denied(request):
	return render(request, 'events/permission-denied.html', {})

def page_not_found(request):
	return render(request, 'events/page-not-found.html', {})		

def server_error(request):
	return render(request, 'events/server-error.html', {})

def new_ticket(request):
	ticketForm = TicketForm()
	context = {'message' : '', 'ticketForm': ticketForm}
	return render(request, 'events/ticket-bkp.html', context)	

