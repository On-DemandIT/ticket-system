from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^thanks/(?P<request_type>[0-9]+)/$', views.thanks, name='thanks'),
    url(r'^add/$', views.add, name='add'),
    url(r'^list/$', views.list, name='list'),
    url(r'^new-ticket/$', views.new_ticket, name='new-ticket'),
    url(r'^edit/(?P<event_id>[0-9]+)/$', views.edit, name='edit'),
    url(r'^detail/(?P<event_id>[0-9]+)/$', views.detail, name='detail'),
    url(r'^ticket/(?P<ticket_id>[0-9]+)/$', views.ticket, name='ticket'),
    url(r'^delete/(?P<event_id>[0-9]+)/$', views.delete, name='delete'),
    url(r'^server_error', views.server_error, name='server_error')
]