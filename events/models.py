from django.db import models

class Event(models.Model):
	event_title = models.CharField(max_length=200)
	event_start_date = models.DateTimeField('date published')
	event_end_date = models.DateTimeField('date published')
	
	def __unicode__(self):
		return "{0} {1} {2} {3}".format(self, self.event_title, self.event_start_date, self.event_end_date)

	def __str__(self):
		return self.event_title
		
class Ticket(models.Model):
	ticket_title = models.CharField(max_length=200)
	ticket_price = models.CharField(max_length=20)
	sale_start_date = models.DateTimeField('date published')
	sale_end_date = models.DateTimeField('date published')
	event = models.ForeignKey(Event, on_delete=models.CASCADE)

	def __unicode__(self):
		return "{0} {1} {2} {3} {4}".format(self.pk, self.ticket_title, self.ticket_price, self.sale_start_date, self.sale_end_date)

	def __str__(self):
		return self.ticket_title


