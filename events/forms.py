from django import forms
from .models import Event,Ticket
from datetime import datetime

class BaseForm(forms.Form):
	def __init__(self, *args, **kwargs):
		super(BaseForm, self).__init__(*args, **kwargs)
		#self.fields[ 'event_date' ].input_formats = [ '%m-%d-%Y %H:%M' ]
		for myField in self.fields:
			if 'Date' in self.fields[myField].label:
				self.fields[myField].widget.attrs['class'] = "form-control picker-input"
			else:
				self.fields[myField].widget.attrs['class'] = "form-control"	
			self.fields[myField].widget.attrs['placeholder'] = self.fields[myField].label

class EventForm(BaseForm):
	event_title = forms.CharField(label='Event Title', max_length=100, help_text='Event Title')
	event_start_date = forms.DateTimeField(input_formats=['%m-%d-%Y %H:%M'],label='Event Start Date', help_text='Event Start Date')
	event_end_date = forms.DateTimeField(input_formats=['%m-%d-%Y %H:%M'],label='Event End Date', help_text='Event End Date')

	def save_event(self):
		event_title = self.cleaned_data['event_title']
		event_start_date  = self.cleaned_data['event_start_date'].strftime("%Y-%m-%d %H:%M")
		event_end_date  = self.cleaned_data['event_end_date'].strftime("%Y-%m-%d %H:%M")  
		event = Event(event_title = event_title,event_start_date = event_start_date,event_end_date = event_end_date)
		event.save()
		return event

	def update_event(self, event_id):
		eventDetail = Event.objects.get(id=event_id)
		eventDetail.event_title = self.cleaned_data['event_title']
		eventDetail.event_start_date  = self.cleaned_data['event_start_date'].strftime("%Y-%m-%d %H:%M")
		eventDetail.event_end_date  = self.cleaned_data['event_end_date'].strftime("%Y-%m-%d %H:%M")
		eventDetail.save()
		return eventDetail

class TicketForm(BaseForm):
    ticket_title = forms.CharField(label='Ticket Title',max_length = 250)
    ticket_start_date = forms.DateField(label='Ticket Start Date', required = True)
    ticket_end_date = forms.DateField(label='Ticket End Date', required = True)
    
    def save_ticket(self, event_id):
        ticket_title = self.cleaned_data['ticket_title']
        start_date = self.cleaned_data['ticket_start_date']
        last_date = self.cleaned_data['ticket_end_date']
        ticket = Ticket(ticket_title = ticket_title,start_date = start_date,last_date = last_date,event_id_id = int(event_id))
        ticket.save()
        return ticket		